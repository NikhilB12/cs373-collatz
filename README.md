# CS373: Software Engineering Collatz Repo

* Name: Nikhil Bodicharla

* EID: nb24499

* GitLab ID: NikhilB12

* HackerRank ID: nikhil132

* Git SHA: eb713ea1acabbb9fe23a85ae70b86e91d5b12360

* GitLab Pipelines: https://gitlab.com/NikhilB12/cs373-collatz/pipelines

* Estimated completion time: 5 hours

* Actual completion time: 9 hours

* Comments: It was important to pay attention to all the directions because all the 
steps were meticolous and asked for specific things
