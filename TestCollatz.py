#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_5(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_6(self):
        v = collatz_eval(74, 118)
        self.assertEqual(v, 119)

    def test_eval_7(self):
        v = collatz_eval(46, 493)
        self.assertEqual(v, 144)

    def test_eval_8(self):
        v = collatz_eval(8, 200)
        self.assertEqual(v, 125)

    def test_eval_8(self):
        v = collatz_eval(10, 288)
        self.assertEqual(v, 128)

    def test_eval_9(self):
        v = collatz_eval(11, 46)
        self.assertEqual(v, 112)

    def test_eval_10(self):
        v = collatz_eval(8, 855)
        self.assertEqual(v, 171)

    def test_eval_11(self):
        v = collatz_eval(24, 50)
        self.assertEqual(v, 112)

    def test_eval_12(self):
        v = collatz_eval(11, 21)
        self.assertEqual(v, 21)

    def test_eval_13(self):
        v = collatz_eval(27, 56)
        self.assertEqual(v, 113)

    def test_eval_14(self):
        v = collatz_eval(4, 85)
        self.assertEqual(v, 116)

    def test_eval_15(self):
        v = collatz_eval(130, 143)
        self.assertEqual(v, 104)

    def test_eval_16(self):
        v = collatz_eval(1, 175)
        self.assertEqual(v, 125)

    def test_eval_17(self):
        v = collatz_eval(40, 137)
        self.assertEqual(v, 122)

    def test_eval_18(self):
        v = collatz_eval(40, 137)
        self.assertNotEqual(v, 132)

    def test_eval_19(self):
        v = collatz_eval(27, 56)
        self.assertNotEqual(v, 343)

    def test_eval_20(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_20(self):
        v = collatz_eval(1000000, 1000000)
        self.assertEqual(v, 153)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
